# SFTI prototype study

This document describes how the prototype works.

## Prerequisites
jdk1.8, doop-4.20.32, souffle-1.7.1-27


## Structure
- serviceDriverGenerator/. A project for generating drivers for service providers.
main class: nz.ac.massey.cs.DriverGenerator
parameter:args[0]=input Dir, args[1]=output Dir
- data/providers. 31 programs that contains services meta info
- [debl.](https://bitbucket.org/jensdietrich/debl-experiments/src/master/debl/ "debl ") (branch:serviceLoader) A transformation tool to transfrom services to implmenations. 
Usage: --in dir_to_input --out dir_to_output --transformers service
- data/results. Doop results, contains reachable methods BEFORE and AFTER transformation
Usage: TODO
