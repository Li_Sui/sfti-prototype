
public class Test {

    public static void main(String[] args) throws Exception{
        java.util.Iterator serviceIterator = java.util.ServiceLoader.load(ServiceIF.class).iterator();
        while(serviceIterator.hasNext()) {
            //put a breakpoint here to inspect Object MyService is created or not.
            System.out.println("before service init");
            ServiceIF service = (ServiceIF) serviceIterator.next();
            //after next(), Object Myservice is created.
            System.out.println("after service init");
        }
    }
}
