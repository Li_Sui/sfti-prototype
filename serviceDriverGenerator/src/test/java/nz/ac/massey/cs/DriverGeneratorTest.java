package nz.ac.massey.cs;

import com.google.common.collect.Multimap;
import javassist.ClassPool;
import org.junit.Assert;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Test for getting correct service interface from meta data
 * @author li sui
 */
public class DriverGeneratorTest {

    @org.junit.Test
    public void testGatherServiceInfo() throws Exception{
        Multimap<String,String> result= DriverGenerator.gatherServiceInfo(new File("src/test/"), ClassPool.getDefault());
        String expectedJarFileName="testService";
        String expectedServiceInterface="ServiceIF";
        String key=result.keySet().iterator().next();
        String value=result.get(key).iterator().next();
        Assert.assertEquals(expectedJarFileName,key);
        Assert.assertEquals(expectedServiceInterface,value);
    }
}