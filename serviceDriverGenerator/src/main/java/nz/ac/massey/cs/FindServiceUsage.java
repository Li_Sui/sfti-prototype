package nz.ac.massey.cs;
import javassist.*;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * utility class for finding ServiceLoader.load() callsite. This class is used for manually inspecting ServiceLoader.load()
 *
 * @author li sui
 */
public class FindServiceUsage {
    static List<String> metaData;
    public static void main(String[] args) throws Exception{
        if(args.length==0){
            System.err.println("args[0]=input Dir");
            return;
        }
        String inputDir =args[0];
        metaData=find(inputDir);

        // iterate a dir to find all zips and jars
        Iterator it =FileUtils.iterateFiles(new File(inputDir),null,true);

        while(it.hasNext()){
            File f = (File) it.next();
            parseZip(f);
        }
    }

    private static void parseZip(File f) throws Exception{
        if(f.getName().endsWith(".jar") || f.getName().endsWith(".zip")){
            ZipFile zipFile = new ZipFile(f.getPath());

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while(entries.hasMoreElements()){
                ZipEntry entry = entries.nextElement();
                if(entry.getName().endsWith(".class")) {
                    InputStream stream = zipFile.getInputStream(entry);
                    readClass(IOUtils.toByteArray(stream), f.getName(),entry.getName());
                }
            }

        }
    }

    private static void readClass(byte[] b,String jarName, String fileName){
        ClassPool pool = ClassPool.getDefault();
        CtClass clazz = null;

        try {
            clazz = pool.makeClass(new ByteArrayInputStream(b));
            CtBehavior[] methods = clazz.getDeclaredBehaviors();

            for (int i = 0; i < methods.length; i++) {
                    final String methodName=methods[i].getName();
                    methods[i].instrument( new ExprEditor() {

                        public void edit(MethodCall m)
                                throws CannotCompileException
                        {
                            if(m.getClassName().equals("java.util.ServiceLoader") && m.getMethodName().equals("load")){
                                System.out.println(jarName+"-"+fileName+"-"+methodName+"------"+m.getClassName()+"."+m.getMethodName());
                            }
                        }
                    });


            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (clazz != null) {
                clazz.detach();
            }
        }
    }

    public static List<String> find(String inputDir) throws Exception{
        Iterator it =  FileUtils.iterateFiles(new File(inputDir),null,true);
        List<String> meta=new ArrayList<>();
        while(it.hasNext()){
            File jar=(File)it.next();
            if (jar.getName().endsWith(".zip") || jar.getName().endsWith(".jar")) {
                ZipFile zipFile = new ZipFile(jar);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().contains("META-INF/services/") && !entry.isDirectory()) {
                        String serviceName = entry.getName().split("META-INF/services/")[1];
                        meta.add(serviceName);
                        System.out.println(jar.getAbsolutePath()+ ":" + entry.getName());
                    }
                }
            }
        }
        return meta;
    }
}


