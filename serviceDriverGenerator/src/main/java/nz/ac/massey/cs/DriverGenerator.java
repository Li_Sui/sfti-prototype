package nz.ac.massey.cs;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.sun.org.apache.xerces.internal.impl.xpath.XPath;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;


import java.io.File;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * class for generating a Driver for each service meta info discovered.
 * args[0]=input Dir, args[1]=output Dir
 *
 * @author li sui
 */
public class DriverGenerator {

    public static void main(String[] args) throws Exception{
        //args[0]=input dir
        if(args.length==0){
            System.err.println("args[0]=input Dir, args[1]=output Dir");
            return;
        }
        ClassPool pool = ClassPool.getDefault();

        String inputDir=args[0];
        String outputDir=args[1];
        Multimap<String,String> map=gatherServiceInfo(new File(inputDir),pool);
        generateClass(pool,map,outputDir);
    }

    public static Multimap<String,String> gatherServiceInfo(File dir,ClassPool pool ) throws Exception{
        Iterator it =  FileUtils.iterateFiles(dir,null,true);
        Multimap<String,String> map= ArrayListMultimap.create();
        while(it.hasNext()){
            File jar=(File)it.next();
            if (jar.getName().endsWith(".zip") || jar.getName().endsWith(".jar")) {
                ZipFile zipFile = new ZipFile(jar);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                pool.appendClassPath(jar.getAbsolutePath());
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().contains("META-INF/services/") && !entry.isDirectory()) {
                        String serviceName = entry.getName().split("META-INF/services/")[1];
                        map.put(FilenameUtils.removeExtension(jar.getName()),serviceName);
                    }
                }
            }
        }

        return map;
    }

    public static void generateClass(ClassPool pool,Multimap<String,String> map, String outputDir) throws Exception{
        for(String jar: map.keySet()){
            CtClass cc = pool.makeClass("Driver");
            StringBuffer method = new StringBuffer();
            method.append("public static void main(String[] args){ ");
            for(int i=0;i<map.get(jar).size();i++){
                String service =(String)((List)map.get(jar)).get(i);
                method.append("java.util.Iterator serviceIterator"+i+"= java.util.ServiceLoader.load("+service+".class).iterator();");
                method.append("while(serviceIterator"+i+".hasNext()) {" +
                        "            serviceIterator"+i+".next();" +
                        "        }");
            }
            method.append("}");
            try {
                cc.addMethod(CtMethod.make(method.toString(), cc));
                cc.writeFile(outputDir + "/" + jar);
                cc.defrost();
            }catch (CannotCompileException e){
                System.err.println(e.getMessage()+"-in jar:"+jar);
            }
        }
    }
}
